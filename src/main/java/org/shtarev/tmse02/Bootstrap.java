package org.shtarev.tmse02;

import org.shtarev.tmse02.entyty.Project;
import org.shtarev.tmse02.entyty.Task;
import org.shtarev.tmse02.entyty.User;
import org.shtarev.tmse02.repository.*;
import org.shtarev.tmse02.service.*;
import org.shtarev.tmse02.Сommands.AbstractCommand;
import org.shtarev.tmse02.Сommands.ServiceLocator;
import org.shtarev.tmse02.Сommands.UserRole;

import java.util.*;
import java.util.stream.Collectors;

import static org.shtarev.tmse02.Сommands.UserRole.ADMIN;
import static org.shtarev.tmse02.Сommands.UserRole.REGULAR_USER;

public final class Bootstrap implements ServiceLocator {
    private final TaskRepository<Task> taskRepository = new TaskRepositoryImpl();
    private final ProjectRepository<Project> projectRepository = new ProjectRepositoryImpl();
    private final UserRepository<User> userRepository = new UserRepositoryImpl();
    private final UserService<User> userService = new UserServiceImpl(userRepository);
    private final TaskService<Task> taskService = new TaskServiceImpl(taskRepository);
    private final ProjectService<Project> projectService = new ProjectServiceImpl(projectRepository,taskRepository);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public User bootUser = new User();
    public final Scanner terminalService = new Scanner(System.in);

    @Override
    public ProjectService<Project> getProjectService() {
        return projectService;
    }

    @Override
    public TaskService<Task> getTaskService() {
        return taskService;
    }

    @Override
    public UserService<User> getUserService() {
        return userService;
    }

    @Override
    public User getBootUser() {
        return bootUser;
    }

    @Override
    public void setBootUser(User thisUser) {
        bootUser = thisUser;
    }

    @Override
    public Scanner getTerminalService() {
        return terminalService;
    }

    final protected void createTwoUser() {
        final UserRole[] userRole = {ADMIN};
        final UserRole[] userRole2 = {REGULAR_USER};
        userService.create("admin", "admin", userRole);
        userService.create("regularUser", "regularUser", userRole2);

    }

    protected void init() {
        try {
            for (final Class cl : AppPTManager.classes) {
                Object o = cl.newInstance();
                if (o instanceof AbstractCommand) {
                    registry((AbstractCommand) o);
                } else throw new IllegalArgumentException("error");
            }
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    protected void registry(final AbstractCommand command) {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (cliDescription == null || cliDescription.isEmpty())
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    protected void start() {
        try {
            System.out.println("*** WELCOME TO TASK MANAGER ***");
            String command = "";
            while (!"exit".equals(command)) {
                try {
                    try {
                        List<UserRole> listRole = Arrays.stream(bootUser.getUserRole()).collect(Collectors.toList());
                        System.out.print("Role: " + listRole.stream().findAny() +
                                " |  Enter the terminal command: ");
                    } catch (Exception e) {
                        UserRole[] x = {UserRole.NO_ROLE};
                        bootUser.setUserRole(x);
                        continue;
                    }
                    command = terminalService.nextLine();
                    execute(command);
                } catch (Exception e) {
                    System.out.println(e.getMessage() + "Error");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void execute(final String command) throws Exception {
        try {
            final AbstractCommand abstractCommand = commands.get(command);
            UserRole[] userRoleList = abstractCommand.getListRole();
            UserRole[] userRoleList2 = bootUser.getUserRole();
            for (UserRole thisRole : userRoleList) {
                for (UserRole thisRole2 : userRoleList2) {
                    if (thisRole.equals(thisRole2)) {
                        abstractCommand.execute();
                        return;
                    }
                }
            }
        } catch (NullPointerException e) {
            System.out.print(e.getMessage());
            System.out.println(" command entered incorrectly");
        }
    }
}


