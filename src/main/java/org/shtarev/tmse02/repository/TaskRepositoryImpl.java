package org.shtarev.tmse02.repository;

import org.shtarev.tmse02.entyty.Task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskRepositoryImpl implements TaskRepository<Task> {
    private final Map<String, Task> taskMap = new HashMap<>();

    @Override
    public void persist(Task thisTask) {
        if (!taskMap.containsValue(thisTask))
            taskMap.put(thisTask.getId(), thisTask);
    }

    @Override
    public void remove(final String id, final String userId) {
        final Task thisTask = taskMap.get(id);
        if (thisTask.getUserId().equals(userId)) {
            taskMap.remove(id);
        }
    }

    @Override
    public List<Task> findAll(final String userId) {
        return taskMap.values().stream().filter(task -> task.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public void removeAll(final String userId) {
        taskMap.values().stream().filter(task -> task.getUserId().equals(userId)).forEach(x -> taskMap.remove(x.getId()));
    }
}


