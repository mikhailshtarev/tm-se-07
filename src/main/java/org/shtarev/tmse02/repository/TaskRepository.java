package org.shtarev.tmse02.repository;

import java.util.List;

public interface TaskRepository<T> {
    void persist(final T thisTask);

    void remove(final String id, final String userID);

    List<T> findAll(final String userID);

    void removeAll(final String userID);
}
