package org.shtarev.tmse02.repository;

import org.shtarev.tmse02.entyty.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


public class ProjectRepositoryImpl implements ProjectRepository<Project> {

    private final HashMap<String, Project> projectMap = new HashMap<>();

    @Override
    public void persist(final Project thisProject) {
        if (!projectMap.containsValue(thisProject))
            projectMap.put(thisProject.getId(), thisProject);
    }

    @Override
    public void remove(final String Id, final String userID) {
        final Project thisProject = projectMap.get(Id);
        if (thisProject.getUserId().equals(userID)) {
            projectMap.remove(Id);
        }
    }

    @Override
    public List<Project> findAll(final String userID) {
        final List<Project> projectListValue = new ArrayList<>(projectMap.values());
        return projectListValue.stream().filter(project -> project.getUserId().equals(userID)).collect(Collectors.toList());
    }

    @Override
    public void removeAll(final String userID) {
        final List<Project> projectListValue = new ArrayList<>(projectMap.values());
        final List<Project> project2List = projectListValue.stream().filter(project -> project.getUserId().equals(userID))
                .collect(Collectors.toList());
        for (Project thisProject : project2List) {
            projectMap.remove(thisProject.getId());
        }
    }
}

