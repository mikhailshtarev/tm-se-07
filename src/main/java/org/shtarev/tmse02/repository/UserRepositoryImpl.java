package org.shtarev.tmse02.repository;

import org.apache.commons.codec.digest.DigestUtils;
import org.shtarev.tmse02.entyty.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepositoryImpl implements UserRepository<User> {

    private final Map<String, User> userMap = new HashMap<>();

    @Override
    public void create(final User thisUser) {
        userMap.put(thisUser.getUserId(), thisUser);
    }

    @Override
    public List<User> getUserList() {
        return new ArrayList<>(userMap.values());
    }

    @Override
    public void rePassword(final String userId, final String newPassword) {
        final User thisUser = userMap.get(userId);
        final String newPassword1 = DigestUtils.md5Hex(newPassword);
        thisUser.setPassword(newPassword1);
    }

    @Override
    public void userUpdate(String name, String thisUserId) {
        final User thisUser = userMap.get(thisUserId);
        thisUser.setName(name);
    }
}
