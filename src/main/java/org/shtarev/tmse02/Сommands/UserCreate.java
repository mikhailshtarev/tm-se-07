package org.shtarev.tmse02.Сommands;

import static org.shtarev.tmse02.Сommands.UserRole.REGULAR_USER;

final public class UserCreate extends AbstractCommand{
    @Override
    public String getName() {
        return "UserCreate";
    }

    @Override
    public String getDescription() {
        return "Create New User";
    }

    @Override
    public UserRole[] getListRole() {
    return UserRole.values();
    }

    @Override
    public void execute() {
        System.out.println("Введите имя нового пользователя: ");
        final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите пароль для этого пользователя: ");
        final String password = serviceLocator.getTerminalService().nextLine();
        final UserRole[] userRole = {REGULAR_USER};
        serviceLocator.getUserService().create(name,password,userRole);
    }
}
