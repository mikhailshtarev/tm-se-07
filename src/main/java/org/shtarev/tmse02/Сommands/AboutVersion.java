package org.shtarev.tmse02.Сommands;

import org.apache.log4j.PropertyConfigurator;

public class AboutVersion extends AbstractCommand {

    @Override
    public String getName() {
        return "AboutVersion";
    }

    @Override
    public String getDescription() {
        return "Show version this program";
    }

    @Override
    public UserRole[] getListRole() {
        return UserRole.values();
    }

    @Override
    public void execute() {
        String log4jConfPath = "src/main/resources/log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        System.out.println("Task/Project manager Version:  1.0 ");
    }
}
