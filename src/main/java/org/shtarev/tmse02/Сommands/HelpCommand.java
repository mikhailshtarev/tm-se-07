package org.shtarev.tmse02.Сommands;

import java.util.List;

public final class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public UserRole[] getListRole() {
        return UserRole.values();
    }

    @Override
    public void execute() {
        final List<AbstractCommand> lists = serviceLocator.getCommands();
        lists.forEach(abstractCommand -> System.out.println("Название команды: " + abstractCommand.getName() +
                "  Описание команды: " + abstractCommand.getDescription()));
    }
}
