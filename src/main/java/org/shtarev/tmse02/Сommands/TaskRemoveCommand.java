package org.shtarev.tmse02.Сommands;

final public class TaskRemoveCommand extends AbstractCommand{
    @Override
    public String getName() {
        return "TaskRemove";
    }

    @Override
    public String getDescription() {
        return "Remove selected Task";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите ID задачи которую хотите удалить:");
        final String taskId = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getTaskService().delete(taskId,serviceLocator.getBootUser().getUserId());
    }
}
