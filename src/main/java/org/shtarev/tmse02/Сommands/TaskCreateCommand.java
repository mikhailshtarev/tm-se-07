package org.shtarev.tmse02.Сommands;

final public class TaskCreateCommand extends AbstractCommand{
    @Override
    public String getName() {
        return "TaskCreate";
    }

    @Override
    public String getDescription() {
        return "Create new Task";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите название задачи:");
        final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите описание задачи:");
        final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала задачи: ");
        final String dataStart = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания задачи: ");
        final String dataFinish = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите ID проекта, к которому относится задача: ");
        final  String projectID = serviceLocator.getTerminalService().nextLine();
        final  String thisUserID = serviceLocator.getBootUser().getUserId();
        serviceLocator.getTaskService().create(name, description, dataStart, dataFinish, projectID,thisUserID);

    }
}
