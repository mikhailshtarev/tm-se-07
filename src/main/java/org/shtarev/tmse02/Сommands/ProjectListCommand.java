package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.Project;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "ProjectList";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute()  {
        System.out.println("[PROJECT LIST]");
        final List<Project> projectList = serviceLocator.getProjectService().projectsReadAll(serviceLocator.getBootUser()
                .getUserId());
        projectList.forEach(project -> System.out.println("Название проекта: " + project.getName() + "  ID проекта: " + project.getId()+
                "  ID User: " + project.getUserId()));
    }
}
