package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.Project;
import org.shtarev.tmse02.entyty.Task;
import org.shtarev.tmse02.entyty.User;
import org.shtarev.tmse02.service.ProjectService;
import org.shtarev.tmse02.service.TaskService;
import org.shtarev.tmse02.service.UserService;

import java.util.List;
import java.util.Scanner;

public interface ServiceLocator {

    ProjectService<Project> getProjectService();

    TaskService<Task> getTaskService();

    List<AbstractCommand> getCommands();

    UserService<User> getUserService();

    User getBootUser();

    void setBootUser(User thisUser);

    Scanner getTerminalService();
}
