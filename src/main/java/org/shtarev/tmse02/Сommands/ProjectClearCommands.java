package org.shtarev.tmse02.Сommands;


public final class ProjectClearCommands extends AbstractCommand {
    @Override
    public String getName() {
        return "ProjectClear";
    }

    @Override
    public String getDescription() {
        return "Delete all Project with her tasks";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        serviceLocator.getProjectService().projectDeleteAll(serviceLocator.getBootUser().getUserId());
    }
}
