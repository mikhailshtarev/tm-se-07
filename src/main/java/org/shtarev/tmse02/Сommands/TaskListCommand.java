package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.Task;

import java.util.List;

final public class TaskListCommand extends AbstractCommand{
    @Override
    public String getName() {
        return "TaskList";
    }

    @Override
    public String getDescription() {
        return "Show all Tasks";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        final List<Task> taskList = serviceLocator.getTaskService().taskReadAll(serviceLocator.getBootUser().getUserId());
        taskList.forEach(task -> System.out.println("Название задачи: " + task.getName() + "  ID задачи:  " + task.getId() +
                "  ID проекта к которому относится задача:  " + task.getProjectId()));

    }
}
