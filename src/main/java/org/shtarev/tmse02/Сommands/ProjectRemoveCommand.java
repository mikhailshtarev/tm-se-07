package org.shtarev.tmse02.Сommands;

final public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "ProjectRemove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project by its ID";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите ID проекта который хотите удалить:");
        final String projectID = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getProjectService().delete(projectID,serviceLocator.getBootUser().getUserId());
    }
}
