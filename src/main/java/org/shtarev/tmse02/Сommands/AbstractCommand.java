package org.shtarev.tmse02.Сommands;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract UserRole[] getListRole();

    public abstract void execute() throws Exception;
}
