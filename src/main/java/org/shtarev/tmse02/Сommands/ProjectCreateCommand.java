package org.shtarev.tmse02.Сommands;


public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "ProjectCreate";
    }

    @Override
    public String getDescription() {
        return "Create a new Project";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите название проекта,который хотите создать: ");
        final String projectName = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите описание проекта: ");
        final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала проекта: ");
        final String dateStartSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания проекта: ");
        final String dateFinishSP = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getProjectService().create(projectName, description, dateStartSP, dateFinishSP,serviceLocator.getBootUser().getUserId());

    }
}
