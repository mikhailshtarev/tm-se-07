package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.User;

final public class UserLogOut extends AbstractCommand{
    @Override
    public String getName() {
        return "UserLogout";
    }

    @Override
    public String getDescription() {
        return "User ends the session";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        serviceLocator.setBootUser(new User());
    }
}
