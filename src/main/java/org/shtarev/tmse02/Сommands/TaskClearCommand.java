package org.shtarev.tmse02.Сommands;

final public class TaskClearCommand extends AbstractCommand{
    @Override
    public String getName() {
        return "TaskClear";
    }

    @Override
    public String getDescription() {
        return "Remove all Tasks";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().taskDeleteAll(serviceLocator.getBootUser().getUserId());
    }
}
