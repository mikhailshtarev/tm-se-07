package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.User;

import java.util.List;

final public class UserList extends AbstractCommand {
    @Override
    public String getName() {
        return "UserList";
    }

    @Override
    public String getDescription() {
        return "View all Users";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        final List<User> userNameList = serviceLocator.getUserService().getUserList();
        userNameList.forEach(user -> System.out.println("Имя пользователя: " + user.getName() + "  ID пользователя: " + user.getUserId()));
    }
}
