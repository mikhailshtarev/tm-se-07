package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.User;

final public class UserUpdate extends AbstractCommand{
    @Override
    public String getName() {
        return "UserUpdate";
    }

    @Override
    public String getDescription() {
        return "Update user profile";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите новое имя  пользователя:");
        final String name = serviceLocator.getTerminalService().nextLine();
        User thisUser = serviceLocator.getBootUser();
        final String thisUserId=thisUser.getUserId();
        serviceLocator.getUserService().userUpdate(name,thisUserId);
    }
}
