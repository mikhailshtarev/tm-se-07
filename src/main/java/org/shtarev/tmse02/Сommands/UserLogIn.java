package org.shtarev.tmse02.Сommands;

final public class UserLogIn extends AbstractCommand {
    @Override
    public String getName() {
        return "UserLogIn";
    }

    @Override
    public String getDescription() {
        return "Sign in";
    }

    @Override
    public UserRole[] getListRole() {
        return UserRole.values();
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите имя пользователя: ");
        final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите пароль: ");
        final String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.setBootUser(serviceLocator.getUserService().LogIn(name, password));
    }
}
