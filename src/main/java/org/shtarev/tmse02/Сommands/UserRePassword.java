package org.shtarev.tmse02.Сommands;

final public class UserRePassword extends AbstractCommand{
    @Override
    public String getName() {
        return "UserRePassword";
    }

    @Override
    public String getDescription() {
        return "Update you password";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите имя пользователя, у которого хотите поменять пароль: ");
        final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите старый пароль: ");
        final  String oldPassword = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите новый пароль: ");
        final String newPassword = serviceLocator.getTerminalService().nextLine();
        if (!serviceLocator.getBootUser().getName().equals(name))
        {System.out.println("wrong username or password");
        return;}
        serviceLocator.getUserService().rePassword(name, oldPassword,newPassword);
    }
}
