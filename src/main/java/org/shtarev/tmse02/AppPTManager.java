package org.shtarev.tmse02;

import org.shtarev.tmse02.Сommands.*;

public class AppPTManager {

    final static Class[] classes = {HelpCommand.class, ProjectListCommand.class, ProjectCreateCommand.class,
            ProjectClearCommands.class, ProjectRemoveCommand.class,TaskListCommand.class, TaskCreateCommand.class,
            TaskClearCommand.class, TaskRemoveCommand.class, UserList.class,UserCreate.class, UserLogIn.class,
            UserLogOut.class, UserRePassword.class, UserUpdate.class,AboutVersion.class};

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.createTwoUser();
        bootstrap.init();
        bootstrap.start();

    }
}
