package org.shtarev.tmse02.service;

import java.util.List;

public interface ProjectService<T> {

    void create(final String name, final String description, final String startDate, final String finishDate,
                final String thisUserId);

    void delete(final String id, final String userID);

    List<T> projectsReadAll(final String userID);

    void projectDeleteAll(final String userID);
}
