package org.shtarev.tmse02.service;

import org.shtarev.tmse02.entyty.Project;
import org.shtarev.tmse02.entyty.Task;
import org.shtarev.tmse02.repository.ProjectRepository;
import org.shtarev.tmse02.repository.TaskRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.List;

public class ProjectServiceImpl implements ProjectService<Project> {

    private final ProjectRepository<Project> projectRepository;
    private final TaskRepository<Task> taskRepository;
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("dd-MM-uuuu")
            .withResolverStyle(ResolverStyle.STRICT);

    public ProjectServiceImpl(ProjectRepository<Project> projectRepository ,TaskRepository<Task> taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name, final String description, final String startDate, final String finishDate, final String thisUserId) {
        try {
            if (name.isEmpty() || description.isEmpty() || startDate.isEmpty() || finishDate.isEmpty())
                {System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;}
            final LocalDate startDateTR = LocalDate.parse(startDate, dateTimeFormatter);
            final LocalDate finishDateTR = LocalDate.parse(finishDate, dateTimeFormatter);
            final Project thisProject = new Project();
            thisProject.setName(name);
            thisProject.setDescription(description);
            thisProject.setDataStart(startDateTR);
            thisProject.setDataFinish(finishDateTR);
            thisProject.setUserId(thisUserId);
            projectRepository.persist(thisProject);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "!  This project do not create. Error");
        }
             }

    @Override
    public void delete(final String id, final String userID) {
        try {
            if (id.isEmpty()){System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;}
            projectRepository.remove(id, userID);
            taskRepository.remove(id, userID);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "!  This project is not in the list. Error");
        }
    }

    @Override
    public List<Project> projectsReadAll(final String userID) {
        return projectRepository.findAll(userID);
    }

    @Override
    public void projectDeleteAll(final String userId) {
        projectRepository.removeAll(userId);
        taskRepository.removeAll(userId);
    }
}

