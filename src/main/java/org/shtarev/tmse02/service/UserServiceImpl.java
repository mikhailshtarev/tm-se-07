package org.shtarev.tmse02.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.shtarev.tmse02.entyty.User;
import org.shtarev.tmse02.repository.UserRepository;
import org.shtarev.tmse02.Сommands.UserRole;

import java.util.List;

public class UserServiceImpl implements UserService<User> {

    private final UserRepository<User> userRepository;


    public UserServiceImpl(UserRepository<User> userRepository) {
        this.userRepository = userRepository;
    }

    public void create(final String name, final String password, final UserRole[] userRole) {
        try {
            if (name.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            final User thisUser = new User();
            thisUser.setName(name);
            thisUser.setPassword(DigestUtils.md5Hex(password));
            thisUser.setUserRole(userRole);
            userRepository.create(thisUser);
        } catch (
                NullPointerException e) {
            System.out.println(e.getMessage() + "Error");
        }
    }


    public List<User> getUserList() {
        return userRepository.getUserList();
    }

    @Override
    public User LogIn(final String name, final String password) {
        final User newUser = new User();
        try {
            if (name.isEmpty() || password.isEmpty()) {
                System.out.println("One of the fields is empty, please try again");
                return newUser;
            }
            final List<User> userListUL = userRepository.getUserList();
            final String thisPassword = DigestUtils.md5Hex(password);
            for (User user : userListUL) {
                if (user.getName().equals(name) || user.getPassword().equals(thisPassword)) {
                    return user;
                }
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "  !  Incorrect console input, please try again. Error");
        }
        System.out.println("Incorrect name or password, please try again.");
        return newUser;
    }


    public void rePassword(String name, String oldPassword, String newPassword) {
        try {
            if (name.isEmpty() || newPassword.isEmpty()) {
                System.out.println("One of the fields is empty, please try again");
                return;
            }
            final List<User> userCollect = userRepository.getUserList();
            for (User thisUser : userCollect) {
                String oldPassword2 = DigestUtils.md5Hex(oldPassword);
                if (thisUser.getName().equals(name) || thisUser.getPassword().equals(oldPassword2)) {
                    userRepository.rePassword(thisUser.getUserId(), newPassword);
                } else System.out.println("Username or password does not match");
            }
        } catch (
                NullPointerException e) {
            System.out.println(e.getMessage() + "  !  Incorrect console input, please try again. Error");
        }

    }

    public void userUpdate(String name, String thisUserId) {
        try {
            if (name.isEmpty() || thisUserId.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            userRepository.userUpdate(name, thisUserId);
        } catch (
                NullPointerException e) {
            System.out.println(e.getMessage() + "  !  Incorrect console input, please try again. Error");
        }
    }
}

