package org.shtarev.tmse02.service;

import org.shtarev.tmse02.entyty.Task;
import org.shtarev.tmse02.repository.TaskRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.List;

public class TaskServiceImpl implements TaskService<Task> {
    private final TaskRepository<Task> taskRepository;
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("dd-MM-uuuu")
            .withResolverStyle(ResolverStyle.STRICT);

    public TaskServiceImpl(final TaskRepository<Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name, final String description, final String dataStart,
                       final String dataFinish, final String projectID, final String thisUserID) {
        try {
            if (name.isEmpty() || description.isEmpty() || dataStart.isEmpty() || dataFinish.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            final LocalDate dataStartTR = LocalDate.parse(dataStart, dateTimeFormatter);
            final LocalDate dataFinishTR = LocalDate.parse(dataFinish, dateTimeFormatter);
            final Task thisTask = new Task();
            thisTask.setName(name);
            thisTask.setDescription(description);
            thisTask.setDataStart(dataStartTR);
            thisTask.setDataFinish(dataFinishTR);
            thisTask.setProjectId(projectID);
            thisTask.setUserId(thisUserID);
            taskRepository.persist(thisTask);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "! This task do not create. Error");
        }
    }

    @Override
    public void delete(final String id, final String userId) {
        try {
            if (id.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            taskRepository.remove(id, userId);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "!  This project is not in the list. Error");
        }
    }

    @Override
    public List<Task> taskReadAll(final String userId) {
        return taskRepository.findAll(userId);
    }

    @Override
    public void taskDeleteAll(final String userId) {
        taskRepository.removeAll(userId);
    }
}





