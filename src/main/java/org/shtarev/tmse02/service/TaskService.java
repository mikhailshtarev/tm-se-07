package org.shtarev.tmse02.service;

import java.util.List;

public interface TaskService<T> {

    void create(final String name, final String description, final String dataStart,
                final String dataFinish, final String projectID, final String thisUserID);

    void delete(final String id, final String userId);

    List<T> taskReadAll(final String userId);

    void taskDeleteAll(final String userId);
}
