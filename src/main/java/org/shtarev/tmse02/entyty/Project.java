package org.shtarev.tmse02.entyty;

import java.time.LocalDate;
import java.util.UUID;

public class Project {
    private final  String id= UUID.randomUUID().toString();
    private String name;
    private String description;
    private LocalDate dataStart ;
    private LocalDate dataFinish ;
    private String userId;

    final public void setName(String name) { this.name = name;}
    final public void setDescription(String description) { this.description = description;}
    final public void setDataStart(LocalDate dataStart) { this.dataStart = dataStart;}
    final public void setDataFinish(LocalDate dataFinish) { this.dataFinish = dataFinish;}
    final public String getId() {return id;}
    final public String getName() {
        return name;
    }
    final public String getDescription() {
        return description;
    }
    final public LocalDate getDataStart() {
        return dataStart;
    }
    final public String getUserId() {
        return userId;
    }
    final public void setUserId(String userId) {
        this.userId = userId;
    }

}
