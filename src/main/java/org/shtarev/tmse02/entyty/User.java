package org.shtarev.tmse02.entyty;

import org.shtarev.tmse02.Сommands.UserRole;

import java.util.UUID;

public class User {
    private String name;
    private String password;
    private UserRole[] userRole;
    private final String userId = UUID.randomUUID().toString();

    final public String getName() {
        return name;
    }
    final public void setName(String name) {
        this.name = name;
    }
    final public String getPassword() {
        return password;
    }
    final public void setPassword(String password) {
        this.password = password;
    }
    final public UserRole[] getUserRole() {
        return userRole;
    }
    final public void setUserRole(UserRole[] userRole) {
        this.userRole = userRole;
    }
    final public String getUserId() {
        return userId;
    }
}
